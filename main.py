
"""
This is the interface for DoorMan the Slack Client and texting client to manage door connections


Testing Channel Hook:

https://hooks.slack.com/services/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

Sample:
curl -X POST -H 'Content-type: application/json' --data '{"text":"Hello, World!"}' https://hooks.slack.com/services/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


"""

class DoorMan:

    def __init__(self):
        name = "DoorMan"
        APPID = "AB4FXACF4"
        ClientID = "73879444642.378541352514"
        ClientSecret = "77ba11883dd67a57e8c46ae10fbfa58e"
        VerificationToken = "tidLJx4t26flXZ9MqXNpipQ0"
        team_id  = 'T25RVD2JW'

        DOOR_PROMPT = {
            "text": "Knock Knock!!",
            "attachments": [
                {
                    "text": "Which door would you like to unlock?",
                    "fallback": "You are unable to choose a game",
                    "callback_id": "door_selection",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "actions": [
                        {
                            "name": "door",
                            "text": "Capp",
                            "type": "button",
                            "value": "capp",
                            "confirm": {
                                "title": "Are you sure?",
                                "ok_text": "Yes",
                                "dismiss_text": "No"
                            }
                        },
                        {
                            "name": "door",
                            "text": "Mission",
                            "type": "button",
                            "value": "mission",
                            "confirm": {
                                "title": "Are you sure?",
                                "ok_text": "Yes",
                                "dismiss_text": "No"
                            }
                        }
                    ]
                }
            ]
        }