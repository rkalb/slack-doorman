import os
from flask import abort, Flask, jsonify, request
import json
from pykevoplus import Kevo
import threading,time
import requests


"""
SLACK_VERIFICATION_TOKEN=ti'dLJx4t26flXZ9MqXNpipQ0' SLACK_TEAM_='T25RVD2JW'

Author: Ryan Kalb
Date: 06/12/18
Description: Door Opening Service that interfaces with slack

"""

app = Flask(__name__)


KEVO_USERNAME = ""
KEVO_PASSWORD = ""

DOOR_PROMPT = {
            "text": "Knock Knock!!",
            "attachments": [
                {
                    "text": "Which door would you like to unlock?",
                    "fallback": "You are unable to choose a game",
                    "callback_id": "door_selection",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "actions": [
                        {
                            "name": "door",
                            "text": "Capp St",
                            "type": "button",
                            "value": "capp",
                            "confirm": {
                                "title": "Are you sure?",
                                "ok_text": "Yes",
                                "dismiss_text": "No"
                            }
                        },
                        {
                            "name": "door",
                            "text": "17th St",
                            "type": "button",
                            "value": "17th",
                            "confirm": {
                                "title": "Are you sure?",
                                "ok_text": "Yes",
                                "dismiss_text": "No"
                            }
                        }
                    ]
                }
            ]
        }

pendingDoor = None

def is_request_valid(request):
    is_token_valid = request.form['token'] == os.environ['SLACK_VERIFICATION_TOKEN']
    is_team_id_valid = request.form['team_id'] == os.environ['SLACK_TEAM_ID']

    return is_token_valid and is_team_id_valid


@app.route('/hello-there', methods=['POST'])
def hello_there():
    if not is_request_valid(request):
        abort(400)

    return jsonify(
        response_type='in_channel',
        text='Hello!',
    )

@app.route('/opendoor', methods=['POST'])
def opendoor():
    if not is_request_valid(request):
        abort(400)

    #return jsonify(
    #    response_type='in_channel',
    #    text='Knock Knock!!',
    #    attachments=DOOR_PROMPT
    #)
    print ("Testing OpenDoor Callback")

    return jsonify(DOOR_PROMPT)

@app.route('/event', methods=['POST'])
def event():
    if not is_request_valid(request):
        abort(400)

    #return jsonify(
    #    response_type='in_channel',
    #    text='Knock Knock!!',
    #    attachments=DOOR_PROMPT
    #)
    content = request.get_json(silent=True)
    print (content)

    print ("Testing Event Callback")

    return jsonify(DOOR_PROMPT)

@app.route('/actions', methods=['POST'])
def doorActions():
    print ("Action Recieved!")
   # pool = Pool(processes=1)

    form_json = json.loads(request.form["payload"])
    print form_json
    #print form_json['callback_id']
    if form_json['actions'][0]['name'] == 'door':
        doorSelection = form_json['actions'][0]['value']
        if doorSelection == 'capp':
            thread = threading.Thread(target=unlockDoor, args=(doorSelection, form_json['response_url']))
            thread.start()
            #message = "Unlocking Capp Street Door :door:, {0}".format(locks[0])
            message = "Attempting to Unlock Capp Street Door \n*... Please Wait ....* "
        elif doorSelection == '17th':
            thread = threading.Thread(target=unlockDoor, args=(doorSelection, form_json['response_url']))
            thread.start()
            #message = "Unlocking 17th Street Door :door:, {0}".format(locks[1])
            message = "Attempting to Unlock 17th Street Door \n*... Please Wait ....* "

        else:
            message = "ILLEGAL Door Selection"

    return jsonify(
        # response_type='in_channel',
        text=message,
    )

@app.route('/', methods=['POST'])
def test():
    print ("Testing Callback from Slack Response")
    form_json = json.loads(request.form["payload"])
    print (form_json)
    return jsonify(
        #response_type='in_channel',
        text=form_json,
    )
@app.route('/<path:path>')
def catch_all(path):
    return 'You want path: %s' % path

def unlockDoor(door, response_url):
    if door == 'capp':
        print 'Unlocking Capp'
        pendingDoor = 'capp'
        locks[0].Unlock()
        requests.post(response_url, json={'text': '*Capp St :door: Unlocked!* \nRelocking in 5 Seconds'})
        time.sleep(1)
        locks[0].Lock()
        requests.post(response_url, json={'text': '*Capp St :door: Locked!*'})

    elif door == '17th':
        print 'Unlocking 17th'
        pendingDoor = '17th'
        locks[1].Unlock()
        requests.post(response_url, json={'text': '*17th St :door: Unlocked!* \nRelocking in 5 Seconds'})
        time.sleep(1)
        locks[1].Lock()
        requests.post(response_url, json={'text': '*17th St :door: Locked!*'})
        #requests.post(response_url, json={'text': locks[1].__str__()})

def updateStatus():
    if pendingDoor == 'capp':
        print locks[0]
    if pendingDoor == '17th':
        print locks[1]


# Main Program

locks = Kevo.GetLocks(KEVO_USERNAME, KEVO_PASSWORD)
print locks